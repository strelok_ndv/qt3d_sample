#include "database.h"

Database::Database(QObject *parent) : QObject(parent)
{
    setDefault();
    openDataBase();
}

Database::~Database()
{
    closeDB();
}

void Database::setDefault()
{
    db      = QSqlDatabase::addDatabase("QSQLITE","SQLITE");
    pathDB  = path + nameDB;
    dbError = "";
    qDebug()<< "Database::setDefault() path: " << path;
    qDebug()<< "Database::setDefault() pathDB: " << pathDB;
    qDebug()<< "Database::setDefault() db: "  << db;
}

bool Database::openDataBase()
{
    db.setDatabaseName(pathDB);

    if (!db.open())
    {
        dbError = db.lastError().text();
        qDebug()<<"openDataBase dbError:"<<dbError;
        return false;
    }
    qDebug()<< "openDataBase: "  << db;
    return true;
}

void Database::closeDB()
{
    db.close();
}

QString Database::getLastError()
{
    return dbError;
}
