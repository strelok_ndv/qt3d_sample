#-------------------------------------------------
#
# Project created by QtCreator 2020-06-22T11:18:28
#
#-------------------------------------------------

QT       += 3dcore 3dlogic 3dextras 3dinput sql #core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Scanner
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        database.cpp \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        database.h \
        mainwindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/GetProjections/release/ -lGetProjections
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/GetProjections/debug/ -lGetProjections

#INCLUDEPATH += $$PWD/GetProjections/include
#DEPENDPATH += $$PWD/GetProjections/include


#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/release/ -lSearchObject
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/debug/ -lSearchObject
#else:unix: LIBS += -L$$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/ -lSearchObject

#INCLUDEPATH += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/debug
#DEPENDPATH += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/debug

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/release/libSearchObject.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/debug/libSearchObject.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/release/SearchObject.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/debug/SearchObject.lib
#else:unix: PRE_TARGETDEPS += $$PWD/../../SearchObject/build-SearchObject-Desktop_Qt_5_12_3_MSVC2017_32bit-Debug/libSearchObject.a
