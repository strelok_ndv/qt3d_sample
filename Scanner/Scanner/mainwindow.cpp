#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setDefaultData();
    connect(ui->pbSend, SIGNAL(clicked()), this, SLOT(send()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::send()
{
    qDebug()<<"MainWindow::send()";
    qDebug()<<ui->le_nameFileIn->text();
    qDebug()<<ui->le_nameFileOut->text();
    qDebug()<<ui->dsb_x->value();
    qDebug()<<ui->dsb_y->value();
    qDebug()<<ui->dsb_z->value();
    qDebug()<<ui->dsb_rx->value();
    qDebug()<<ui->dsb_ry->value();
    qDebug()<<ui->dsb_rz->value();
    qDebug()<<ui->dsb_rangle->value();
    qDebug()<<ui->dsb_scale->value();
    //qDebug()<<ui->dsb_angle_x->value();
    //qDebug()<<ui->dsb_angle_y->value();
    //qDebug()<<ui->dsb_angle_z->value();

    nameFileIn = ui->le_nameFileIn->text();
    nameFileOut= ui->le_nameFileOut->text();
    x = ui->dsb_x->value();
    y = ui->dsb_y->value();
    z = ui->dsb_z->value();
    rx = ui->dsb_rx->value();
    ry = ui->dsb_ry->value();
    rz = ui->dsb_rz->value();
    rangle = ui->dsb_rangle->value();
    scale = ui->dsb_scale->value();
    //angle_x = ui->dsb_angle_x->value();
    //angle_y = ui->dsb_angle_y->value();
    //angle_z = ui->dsb_angle_z->value();

    getProjections = GetInstance(pathDir, nameFileIn, nameFileOut, x, y, z, rx, ry, rz, rangle, scale);
                                 //angle_x, angle_y, angle_z);
}

void MainWindow::setDefaultData()
{
    ui->le_nameFileIn->setText(nameFileIn);
    ui->le_nameFileOut->setText(nameFileOut);
    ui->dsb_x->setValue(x);
    ui->dsb_y->setValue(y);
    ui->dsb_z->setValue(z);
    ui->dsb_rx->setValue(rx);
    ui->dsb_ry->setValue(ry);
    ui->dsb_rz->setValue(rz);
    ui->dsb_rangle->setValue(rangle);
    ui->dsb_scale->setValue(scale);
    //ui->dsb_angle_x->setValue(angle_x);
    //ui->dsb_angle_y->setValue(angle_y);
    //ui->dsb_angle_z->setValue(angle_z);
}
