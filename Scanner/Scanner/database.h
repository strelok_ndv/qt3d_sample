#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QDir>
#include <QDebug>

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = nullptr);
    ~Database();
private:
    const QString nameDB = "StructureDB";

    ///путь к рабочей папке
    QString      path;

    ///путь к файлу БД
    QString      pathDB;

    ///объект для работы БД
    QSqlDatabase db;

    ///строка с ошибкой записи в БД
    QString      dbError;

    ///установка значений по умолчанию для класса
    void setDefault();

    ///закрытие прямого соединения с БД
    void closeDB();

    ///функция открытия прямого соединения с БД
    bool openDataBase();

    ///функция получает строкое значение последней ошибки БД
    QString getLastError();

signals:

public slots:

};

#endif // DATABASE_H
