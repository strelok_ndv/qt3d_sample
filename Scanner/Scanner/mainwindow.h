#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "database.h"
#include <GetProjections/include/getProjectionsInterface.h>

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    GetProjectionsInterface *getProjections;
    //Database *m_database = nullptr;
    Database m_database;

    QString  pathDir     = "D:/Projects/QT/XR/qt3d_sample/Scanner/GetProjections/";
    QString  nameFileIn  = "D:/Projects/QT/XR/qt3d_sample/Scanner/GetProjections/Cow.obj";
    QString  nameFileOut = "D:/Projects/QT/XR/qt3d_sample/Scanner/GetProjections/CowProjection.png";
    float    x  = 0.0f,  y = 0.0f,  z = 0.0f;
    float    rx = 1.0f, ry = 1.0f, rz = 1.0f;
    float    rangle = 45.0f;
    float    scale  = 2.0f;
    //float angle_x = 4.0, angle_y = 5.0, angle_z = 6.0;

private:
    Ui::MainWindow *ui;
    void setDefaultData();

public slots:
    void send();
};

#endif // MAINWINDOW_H
