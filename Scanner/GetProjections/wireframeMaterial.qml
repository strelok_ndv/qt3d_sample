/*import Qt3D.Core 2.0
import Qt3D.Render 2.0

Material {
    id: root

    property color ambient:  Qt.rgba( 0.05, 0.05, 0.05, 1.0 )
    property color diffuse:  Qt.rgba( 0.7, 0.7, 0.7, 1.0 )
    property color specular: Qt.rgba( 0.95, 0.95, 0.95, 1.0 )
    property real  shininess: 150.0
    property real  lineWidth: 0.8
    property color lineColor: Qt.rgba( 0.0, 0.0, 0.0, 1.0 )

    parameters: [
        Parameter { name: "ka";  value: Qt.vector3d(root.ambient.r, root.ambient.g, root.ambient.b) },
        Parameter { name: "kd";  value: Qt.vector3d(root.diffuse.r, root.diffuse.g, root.diffuse.b) },
        Parameter { name: "ksp"; value: Qt.vector3d(root.specular.r, root.specular.g, root.specular.b) },
        Parameter { name: "shininess"; value: root.shininess },
        Parameter { name: "line.width"; value: root.lineWidth },
        Parameter { name: "line.color"; value: root.lineColor }
    ]
}*/

import Qt3D.Core 2.0
import Qt3D.Render 2.0

Effect {
    id: root

    parameters: [
        Parameter { name: "ka";   value: Qt.vector3d( 0.1, 0.1, 0.1 ) },
        Parameter { name: "kd";   value: Qt.vector3d( 0.7, 0.7, 0.7 ) },
        Parameter { name: "ks";  value: Qt.vector3d( 0.95, 0.95, 0.95 ) },
        Parameter { name: "shininess"; value: 150.0 }
    ]

    techniques: [
        Technique {
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }

            filterKeys: [ FilterKey { name: "renderingStyle"; value: "forward" } ]

            parameters: [
                Parameter { name: "light.position"; value: Qt.vector4d( 0.0, 0.0, 0.0, 1.0 ) },
                Parameter { name: "light.intensity"; value: Qt.vector3d( 1.0, 1.0, 1.0 ) },
                Parameter { name: "line.width"; value: 1.0 },
                Parameter { name: "line.color"; value: Qt.vector4d( 1.0, 1.0, 1.0, 1.0 ) }
            ]

            renderPasses: [
                RenderPass {
                    shaderProgram: ShaderProgram {
                        vertexShaderCode:   loadSource(Qt.resolvedUrl("glsl/robustwireframe.vert"))
                        geometryShaderCode: loadSource(Qt.resolvedUrl("glsl/robustwireframe.geom"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("glsl/robustwireframe.frag"))
                        //vertexShaderCode:   loadSource("qrc:/shaders/robustwireframe.vert")
                        //geometryShaderCode: loadSource("qrc:/shaders/robustwireframe.geom")
                        //fragmentShaderCode: loadSource("qrc:/shaders/robustwireframe.frag")
                    }
                }
            ]
        }
    ]
}
