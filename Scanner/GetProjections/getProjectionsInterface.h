#ifndef GETPROJECTIONSINTERFACE_H
#define GETPROJECTIONSINTERFACE_H
//#include "structtransfer.h"

#if defined (GETPROJECTION_LIBRARY)
#  define GETPROJECTIONSHARED_API __declspec(dllexport)
#else
#  define GETPROJECTIONSHARED_API __declspec(dllimport)
#endif

#include <QString>

class GetProjectionsInterface
{
    GetProjectionsInterface& operator=(const GetProjectionsInterface&) = delete;

public:
    virtual void Delete() = 0;  // удаляет объект
    //virtual TrStrGeneral* getTrStrGeneral()        = 0;
protected:
    virtual ~GetProjectionsInterface() = default;
};

extern "C" GETPROJECTIONSHARED_API
GetProjectionsInterface* GetInstance(const QString& _pathDir, const QString& _nameFileIn, const QString& _nameFileOut,
                                     const float& _x,  const float& _y,  const float& _z,
                                     const float& _rx, const float& _ry, const float& _rz,
                                     const float& _rangle, const float& _scale);
                                     //const float& _angle_x, const float& _angle_y, const float& _angle_z);

#endif // GETPROJECTIONSINTERFACE_H
