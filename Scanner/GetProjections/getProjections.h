#ifndef GETPROJECTIONS_H
#define GETPROJECTIONS_H

//#include "getProjections_global.h"
#include "getProjectionsInterface.h"
#include "widget3d.h"
//#include "../../../Projects/QT/XR/qt3d_sample/Scanner/offscreenengine.h"

#include <QQuickView>
#include <QQmlComponent>
#include <QString>
#include <QDebug>
#include <QFile>
#include <QTimer>
#include <QCoreApplication>
#include <QOpenGLFunctions_4_0_Core>
#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
//#include <Qt3DCore/QAspectEngine>
//#include <Qt3DRender/QSceneLoader>
#include <Qt3DRender/QMesh>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DRender/QRenderCapture>
#include <Qt3DRender/QRenderAspect>
#include <Qt3DRender/QRenderSettings>
#include <Qt3DRender/QRenderSurfaceSelector>
#include <Qt3DRender/QEffect>
#include <Qt3DExtras/Qt3DWindow>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QDiffuseSpecularMaterial>
//#include <Qt3DExtras/QGoochMaterial>
#include <Qt3DExtras/QOrbitCameraController>
//#include <Qt3DInput/QInputAspect>
//#include <QPropertyAnimation>
//#include <QOffscreenSurface>
//#include <Qt3DLogic/QLogicAspect>
//#include <QEventLoop>
#include <QtWidgets/QWidget>

//class Widget3d;

class GetProjections : public GetProjectionsInterface//, public QWidget
{
    //Q_OBJECT
public:    
    GetProjections(const QString& _pathDir, const QString& _nameFileIn, const QString& _nameFileoUT,
                   const float& _x,  const float& _y,  const float& _z,
                   const float& _rx, const float& _ry, const float& _rz,
                   const float& _rangle, const float& _scale);
                   //const float& _angle_x, const float& _angle_y, const float& _angle_z);
    ~GetProjections() override;

    //Widget3d window;
    //Widget3d *window;
    //QWidget *window;
    ///путь к рабочей папке
    //QString  path;

    ///путь к лог-файлу
    //QString  pathFLog;

public:
    void Delete() override {  delete this;}
    bool getDataInFile(const QString& _nameFileIn, QVector<QVector3D>& out_vertices,
                       QVector<QVector2D>& out_uvs, QVector<QVector3D>& out_normals);
    //Qt3DCore::QEntity* createScene();
    void createScene();

private:    
    QString  pathDir;
    QString  nameFileIn;
    QString  nameFileOut;
    float    x, y, z;
    float    rx, ry, rz;
    float    rangle;
    float    scale;
    //float       angle_x, angle_y, angle_z;

    QFile       fileIn, fileOut;
    QTextStream out, in;

    QString  pathQml;
    QString  pathFileIn;
    QString  pathFileOut;

    Qt3DExtras::Qt3DWindow     *view;
    Qt3DCore::QEntity          *rootEntity;
    Qt3DCore::QEntity          *objEntity;
    //Qt3DExtras::QPhongMaterial *material;
    //Qt3DRender::QMaterial      *material;
    //Qt3DRender::QMesh          *objMesh;
    //Qt3DCore::QTransform       *objTransform;
    Qt3DRender::QCamera        *camera;
    Qt3DRender::QRenderCapture *renderCapture;
    QTimer timer;

    /*QVector<QVector3D> out_vertices;
    QVector<QVector2D> out_uvs;
    QVector<QVector3D> out_normals;*/

    ///
    //TrStrGeneral* getTrStrGeneral() override { return (new TrStrGeneral);}
    //int  fromPageCreatePatientCard(TrStrPatient& trStr)   override;

    //const QString nameFileIn  = ".obj";
    const QString nameQmlFile = "wireframeMaterial.qml";
};

GetProjectionsInterface* GetInstance(const QString& _pathDir, const QString& _nameFileIn, const QString& _nameFileOut,
                                     const float& _x,  const float& _y,  const float& _z,
                                     const float& _rx, const float& _ry, const float& _rz,
                                     const float& _rangle, const float& _scale)
                                     //const float& _angle_x, const float& _angle_y, const float& _angle_z);)
{
    GetProjectionsInterface *getProgectionInterface = new GetProjections(_pathDir, _nameFileIn, _nameFileOut,
                                                                         _x,  _y,  _z,
                                                                         _rx, _ry, _rz,
                                                                         _rangle, _scale);
                                                                         //_angle_x, _angle_y, _angle_z););
    return getProgectionInterface;
};
#endif // GETPROJECTIONS_H
