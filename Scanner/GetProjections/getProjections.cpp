#include "getProjections.h"

GetProjections::GetProjections(const QString& _pathDir, const QString& _nameFileIn, const QString& _nameFileOut,
                               const float& _x, const float& _y, const float& _z,
                               const float& _rx, const float& _ry, const float& _rz,
                               const float& _rangle, const float& _scale) :
                               //const float& _angle_x, const float& _angle_y, const float& _angle_z) :
    pathDir(_pathDir),
    nameFileIn(_nameFileIn),
    nameFileOut(_nameFileOut),
    x(_x), y(_y), z(_z),
    rx(_rx), ry(_ry), rz(_rz),
    rangle(_rangle), scale(_scale)
    //angle_x(_angle_x),
    //angle_y(_angle_y),
    //angle_z(_angle_z)
{
    qDebug()<<"GetProjections::GetProjections()";
    qDebug()<< nameFileIn << nameFileOut
            << x << y << z
            << rx << ry << rz
            << rangle << scale;
            //<< angle_x << angle_y << angle_z;

    QStringList paths = QCoreApplication::libraryPaths();
    paths.append(pathDir);
    paths.append(".");
    //paths.append("imageformats");
    paths.append("platforms");
    QCoreApplication::setLibraryPaths(paths);

    //QCoreApplication::addLibraryPath(path);
    //qDebug()<<"addLibraryPath path: "<<path;
    int k = 0;
    for(auto i : paths)
    {
        qDebug()<<"path"<<k<<": "<<i;
        k++;
    }
    //pathQml     = "D:/Projects/QT/XR/qt3d_sample/GetProjections/" + nameQmlFile;
    pathQml     = pathDir + "include/" + nameQmlFile;
    qDebug()<<"pathQml: "<<pathQml;

    pathFileIn  = /*pathDir +*/ nameFileIn;
    pathFileOut = /*pathDir +*/ nameFileOut;

/*
    fileIn.setFileName(_nameFileIn);
    if (!fileIn.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"GetProjections::getDataInFile: error open file: "<<fileIn.error();
    }
    fileOut.setFileName(_nameFileOut);
    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug()<<"GetProjections::getDataInFile: error open file: "<<fileOut.error();
    }*/
    //out.setDevice(&fileIn);
    //getDataInFile(nameFileIn, out_vertices, out_uvs, out_normals);
    createScene();
}

GetProjections::~GetProjections()
{
    qDebug()<<"GetProjections::~GetProjections()";
    fileIn.close();
    fileOut.close();

    /*delete view;
    delete rootEntity;
    delete objEntity;
    delete material;
    delete objMesh;
    delete objTransform;
    delete camera;
    delete renderCapture;*/

    //delete window;
}

/// создание сцены:
void GetProjections::createScene()
{
    view = new Qt3DExtras::Qt3DWindow();

    Qt3DCore::QEntity *rootEntity = new Qt3DCore::QEntity;
    Qt3DCore::QEntity *objEntity  = new Qt3DCore::QEntity(rootEntity);

    /*QQuickView view;
    view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    QQmlComponent component(view.engine(), QUrl::fromLocalFile("MyItem.qml"));
    QObject *object = component.create();
    object->setParent(view.rootObject());
    view.show();*/


    QQuickView viewQml;
    viewQml.setSource(QUrl(pathQml));
    QQmlComponent component(viewQml.engine(),
                            QUrl::fromLocalFile(pathQml),
                            QQmlComponent::PreferSynchronous );
    qDebug()<<"viewQml.status():"<<viewQml.status();
    QObject *objQml = component.create();
    qDebug()<<"componentQml: "<<objQml;

    Qt3DRender::QMaterial *material = new Qt3DRender::QMaterial();
    //materialQml->setParent(material);

    //Qt3DExtras::QDiffuseSpecularMaterial *material = new Qt3DExtras::QDiffuseSpecularMaterial;
    Qt3DRender::QEffect *wireEffect = new Qt3DRender::QEffect;
    objQml->setParent(wireEffect);
    //objQml->se
    //Qt3DExtras::QPhongMaterial *material = new Qt3DExtras::QPhongMaterial();
    //material->setDiffuse(QColor(254,254,254));
    //materialQml->setParent(material);
    //wireframeEffect->;
    //material->setAmbient(QColor::fromRgba64(0.2, 0.0, 0.0, 1.0));
    //material->setDiffuse(QColor::fromRgba64(0.8, 0.0, 0.0, 1.0));
    material->setEffect(wireEffect);

    Qt3DRender::QMesh *objMesh = new Qt3DRender::QMesh;
    objMesh->setMeshName("ObjMesh");
    objMesh->setSource(QUrl::fromLocalFile(pathFileIn));

    Qt3DCore::QTransform *objTransform = new Qt3DCore::QTransform(objMesh);
    objTransform->setScale(scale);
    objTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(rx,ry,rz), rangle ));//0.0f,0.0f,0.0f), 45.0f));
    objEntity->addComponent(objMesh);
    objEntity->addComponent(objTransform);
    objEntity->addComponent(material);


    qDebug()<<"objMesh->status()_2:"<<objMesh->status();

    // Camera
    //Qt3DRender::QCamera *camera = view.camera();
    Qt3DRender::QCamera *camera = new Qt3DRender::QCamera;
    //camera->setProjectionType( Qt3DRender::QCameraLens::OrthographicProjection );
    camera->lens()->setOrthographicProjection( -20.0f, 20.0f, -20.0f, 10.0f, -1024.0f, 1024.0f );
    camera->setPosition(QVector3D(x, y, z));//0.0f,0.0f,80.0f));
    //camera->setUpVector(QVector3D(0, 1, 0));
    camera->setViewCenter(QVector3D(0, 0, 0));

    /*
    Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(rootEntity);
    Qt3DRender::QPointLight *light = new Qt3DRender::QPointLight(lightEntity);
    light->setColor("white");
    light->setIntensity(0.8f);
    lightEntity->addComponent(light);

    Qt3DCore::QTransform *lightTransform = new Qt3DCore::QTransform(lightEntity);
    lightTransform->setTranslation(QVector3D(60, 0, 40.0f));
    lightEntity->addComponent(lightTransform);
    */


    // Set up the default OpenGL surface format.
    /*QSurfaceFormat format;
    format.setDepthBufferSize(32);
    format.setSamples(8);
    QSurfaceFormat::setDefaultFormat(format);
    // Create the offscreen engine. This is the object which is responsible for handling the 3D scene itself.
    //OffscreenEngine *offscreenEngine = new OffscreenEngine(camera, QSize(1200, 800));
    // Set our scene to be rendered by the offscreen engine.
    //offscreenEngine->setSceneRoot(rootEntity);*/

    view->defaultFrameGraph()->setCamera(camera);
    auto frameGraph = view->defaultFrameGraph();
    frameGraph->dumpObjectTree();
    //auto camSelector = frameGraph->findChild<Qt3DRender::QCamera*>();
    Qt3DRender::QRenderCapture* renderCapture = new Qt3DRender::QRenderCapture(camera);

    qDebug()<<"objMesh->status()_3:"<<objMesh->status();

    view->setRootEntity(rootEntity);
    view->show();

    qDebug()<<"objMesh->status()_4:"<<objMesh->status();

    QString fileImage = pathFileOut;
    QTimer::singleShot( 1000, [renderCapture, fileImage](){
        qDebug()<<"connect(view)";
        Qt3DRender::QRenderCaptureReply* reply = renderCapture->requestCapture();

        QObject::connect(reply, &Qt3DRender::QRenderCaptureReply::completed,
                         [reply, fileImage]{
                             qDebug()<<"connect(reply)";
                             bool k = reply->saveImage(fileImage);
                             reply->deleteLater();                             
                             qDebug()<<"save? - "<<k;
                         });        
    });
}

///получение даты из файла:
bool GetProjections::getDataInFile(const QString& _nameFileIn, QVector<QVector3D>& _out_vertices,
                                   QVector<QVector2D>& _out_uvs, QVector<QVector3D>& _out_normals)
{
    /*
    qDebug()<< "GetProjections::getDataInFile" << "fileName:"<<_nameFileIn;
    QVector<unsigned int> vertexIndices, uvIndices, normalIndices;
    QVector<QVector3D> temp_vertices;
    QVector<QVector2D> temp_uvs;
    QVector<QVector3D> temp_normals;

    while( !out.atEnd() ){
        QStringList lineText;
        QString lineHeader;
        lineText = out.readLine().split(' ');

        // read the first word of the line
        lineHeader = lineText.at(0);

        // else : parse lineText
        if ( lineHeader.compare("v") == 0 ){
            QVector3D vertex;
            vertex.setX(lineText.at(1).toFloat());
            vertex.setY(lineText.at(2).toFloat());
            vertex.setZ(lineText.at(3).toFloat());
            qDebug()<<"vertex:"<<vertex.x()<<";"<<vertex.y()<<";"<<vertex.z();
            temp_vertices.push_back(vertex);
        }else if ( lineHeader.compare("vt") == 0 ){// нет vt
            QVector2D uv;
            uv.setX(lineText.at(1).toFloat());
            uv.setY(lineText.at(2).toFloat());
            qDebug()<<"uv:"<<uv.x()<<";"<<uv.y();
            temp_uvs.push_back(uv);
        }else if ( lineHeader.compare("vn") == 0 ){
            QVector3D normal;
            normal.setX(lineText.at(1).toFloat());
            normal.setX(lineText.at(1).toFloat());
            normal.setX(lineText.at(1).toFloat());
            qDebug()<<"normal:"<<normal.x()<<";"<<normal.y()<<";"<<normal.z();
            temp_normals.push_back(normal);
        }else if (lineHeader.compare("f") == 0 ){
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            for(int i=0; i<3; i++){
                QStringList vertex;
                vertex = lineText.at(i+1).split('/');
                vertexIndex[i] = vertex.at(0).toUInt();
                uvIndex[i]     = vertex.at(1).toUInt();
                normalIndex[i] = vertex.at(2).toUInt();
                qDebug()<<"vertexIndex:"<<"vertexIndex["<<i<<"]="<<vertexIndex[i]
                        <<"uvIndex["<<i<<"]="<<uvIndex[i]
                        <<"normalIndex["<<i<<"]="<<normalIndex[i];
                vertexIndices.push_back(vertexIndex[i]);
                uvIndices.push_back(uvIndex[i]);
                normalIndices.push_back(normalIndex[i]);
            }
            // For each vertex of each triangle
            //for( auto i=0; i<vertexIndices.size(); i++ ){
            //    auto vertexIndex = vertexIndices[i];
            //    out_vertices.push_back(temp_vertices[ vertexIndex-1 ]);
            //}
        }        
    };*/
    qDebug()<<"End file";
    return true;
}


/*int Client::fromPageCreatePatientCard(TrStrPatient& trStr)
{
    qDebug()<< "Client::from pageCreatePatientCard()";
    DllStrPatient dllStr;
    dllStr = trStr;
    m_database->createPatientCard(dllStr);
    dllStr.callbackTrStrPatient(trStr);
    return dllStr.idPatient;
}*/
